/**
 * @file
 * @brief Implementation of [PRNGTester] module
 * @copyright Copyright (c) 2017-2020 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "PRNGTesterModule.hpp"

#include <string>
#include <utility>

#include "core/utils/log.h"

using namespace allpix;

PRNGTesterModule::PRNGTesterModule(Configuration& config, Messenger*, const std::shared_ptr<Detector>& detector)
    : SequentialModule(config, detector), detector_(detector) {
    allow_multithreading();
}

void PRNGTesterModule::run(Event* event) {
    LOG(DEBUG) << "Drawing random number in run() for detector " << detector_->getName();
    LOG(DEBUG) << "\t" << event->getRandomNumber();
}
