/**
 * @file
 * @brief Implementation of [HistoryTester] module
 * @copyright Copyright (c) 2017-2020 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "HistoryTesterModule.hpp"

#include <string>
#include <utility>

#include "core/utils/log.h"
#include "objects/exceptions.h"

using namespace allpix;

HistoryTesterModule::HistoryTesterModule(Configuration& config,
                                         Messenger* messenger,
                                         const std::shared_ptr<Detector>& detector)
    : Module(config, detector), detector_(detector), messenger_(messenger) {

    // ... Implement ... (Typically bounds the required messages and optionally sets configuration defaults)
    // Input required by this module
    messenger_->bindSingle<PixelHitMessage>(this, MsgFlags::REQUIRED);
}

void HistoryTesterModule::initialize() {
    // Get the detector name
    std::string detectorName = detector_->getName();
    LOG(INFO) << "Detector with name " << detectorName;
}

void HistoryTesterModule::run(Event* event) {

    auto message = messenger_->fetchMessage<PixelHitMessage>(this, event);
    LOG(INFO) << "Picked up " << message->getData().size() << " objects from detector " << message->getDetector()->getName();

    auto pixelhits = message->getData();
    for(auto& hit : pixelhits) {
        LOG(INFO) << "PixelHit: " << &hit;
        LOG(INFO) << hit;

        try {
            const auto* charge = hit.getPixelCharge();
            LOG(INFO) << "PixelCharge " << charge;
            LOG(INFO) << *charge;

            auto propagated = charge->getPropagatedCharges();
            LOG(INFO) << propagated.size() << " PropagatedCharges attached";
            for(auto& prop : propagated) {
                LOG(INFO) << "PropagatedCharge: " << prop;
                LOG(INFO) << *prop;

                const auto* depos = prop->getDepositedCharge();
                LOG(INFO) << "DepositedCharge: " << depos;
                LOG(INFO) << *depos;

                const auto* mcp = depos->getMCParticle();
                LOG(INFO) << "MCParticle: " << mcp;
                LOG(INFO) << *mcp;
            }
        } catch(MissingReferenceException&) {
        }
    }
}
