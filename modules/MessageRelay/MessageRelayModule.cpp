/**
 * @file
 * @brief Implementation of MessageRelayModule module
 * @copyright Copyright (c) 2021 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "MessageRelayModule.hpp"

#include <string>
#include <utility>

#include <TClass.h>

#include "core/utils/log.h"

using namespace allpix;

MessageRelayModule::MessageRelayModule(Configuration& config, Messenger* messenger, std::shared_ptr<Detector> detector)
    : Module(config, detector), detector_(detector), messenger_(messenger) {
    // Enable multithreading of this module if multithreading is enabled
    allow_multithreading();

    // Bind to all messages with filter
    messenger_->registerFilter(this, &MessageRelayModule::filter);
}

void MessageRelayModule::initialize() {
    // Read include and exclude list
    if(config_.has("include") && config_.has("exclude")) {
        throw InvalidValueError(config_, "exclude", "include and exclude parameter are mutually exclusive");
    } else if(config_.has("include")) {
        auto inc_arr = config_.getArray<std::string>("include");
        include_.insert(inc_arr.begin(), inc_arr.end());
    } else if(config_.has("exclude")) {
        auto exc_arr = config_.getArray<std::string>("exclude");
        exclude_.insert(exc_arr.begin(), exc_arr.end());
    }
}

bool MessageRelayModule::filter(const std::shared_ptr<BaseMessage>& message, const std::string& message_name) const { // NOLINT
    try {
        const BaseMessage* inst = message.get();
        std::string name_str = " without a name";
        if(!message_name.empty()) {
            name_str = " named " + message_name;
        }
        LOG(TRACE) << "Message relay received " << allpix::demangle(typeid(*inst).name()) << name_str;

        // Get the detector name
        std::string detector_name;
        if(message->getDetector() != nullptr) {
            detector_name = message->getDetector()->getName();
        }

        // Read the object
        auto object_array = message->getObjectArray();
        if(object_array.empty()) {
            return false;
        }
        const Object& first_object = object_array[0];
        std::string class_name = allpix::demangle(typeid(first_object).name());

        // Check if this message should be kept
        if((!include_.empty() && include_.find(class_name) == include_.cend()) ||
           (!exclude_.empty() && exclude_.find(class_name) != exclude_.cend())) {
            LOG(TRACE) << "Message relay ignored message with object " << allpix::demangle(typeid(*inst).name())
                       << " because it has been excluded or not explicitly included";
            return false;
        }
    } catch(MessageWithoutObjectException& e) {
        const BaseMessage* inst = message.get();
        LOG(WARNING) << "Message relay cannot process message of type" << allpix::demangle(typeid(*inst).name())
                     << " with name " << message_name;
        return false;
    }

    return true;
}

void MessageRelayModule::run(Event* event) {

    auto messages = messenger_->fetchFilteredMessages(this, event);
    LOG(DEBUG) << "Relaying " << messages.size() << " messages";

    // Loop through all messages
    for(const auto& [message, name] : messages) {
        auto object_array = message->getObjectArray();
        LOG(DEBUG) << "Relaying message with " << object_array.size() << " objects from detector "
        << message->getDetector()->getName();

        // Dispatch the message
        messenger_->dispatchMessage(this, message, event);
    }
}
