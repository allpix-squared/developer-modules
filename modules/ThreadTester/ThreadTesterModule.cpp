/**
 * @file
 * @brief Implementation of [ThreadTester] module
 * @copyright Copyright (c) 2017-2020 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "ThreadTesterModule.hpp"

#include <string>
#include <utility>

#include "core/utils/log.h"

using namespace allpix;

ThreadTesterModule::ThreadTesterModule(Configuration& config, Messenger*, std::shared_ptr<Detector> detector)
    : Module(config, std::move(detector)) {
    allow_multithreading();
}

void ThreadTesterModule::initialize() {
    LOG(DEBUG) << "initialize()";
}

void ThreadTesterModule::initializeThread() {
    LOG(DEBUG) << "initializeThread()";
}

void ThreadTesterModule::finalize() {
    LOG(DEBUG) << "finalize()";
}

void ThreadTesterModule::finalizeThread() {
    LOG(DEBUG) << "finalizeThread()";
}
