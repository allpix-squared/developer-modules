[![](https://gitlab.cern.ch/allpix-squared/allpix-squared/raw/master/doc/logo_small.png)](https://cern.ch/allpix-squared)

# Allpix<sup>2</sup> Developer Modules

This repository contains additional Allpix<sup>2</sup> modules which help in developing and testing algorithms and the framework.
CMake is used to link against an existing version of Allpix<sup>2</sup>.
For more details about the Allpix<sup>2</sup> project please have a look at the website at https://cern.ch/allpix-squared.

## Contributing
All types of contributions, being it minor and major, are very welcome. Please refer to our [contribution guidelines](https://gitlab.cern.ch/allpix-squared/allpix-squared/tree/master/CONTRIBUTING.md) for a description on how to get started.

Before adding changes it is very much recommended to carefully read through the documentation in the User Manual first.

## Licenses
This software is distributed under the terms of the MIT license. A copy of this license can be found in [LICENSE.md](LICENSE.md).

The documentation is distributed under the terms of the CC-BY-4.0 license. This license can be found in [doc/COPYING.md](doc/COPYING.md).
